<div align="center">
  <img src="./src/assets/logo.svg" width="250" />
</div>

<div align="center">
  <h1>Math Drawer</h1>
</div>

<div align="center">
A simple website for drawing Geometry shapes
</div>

<div align="center">

[![license](https://img.shields.io/badge/license-GNU-blue.svg)](https://gitlab.com/lior43/math-drawer/-/blob/main/LICENSE)
[![license](https://img.shields.io/badge/version-0.1.0-blue.svg)]()
</div>

<div align="center">
  <a href="https://lior43.gitlab.io/math-drawer/">Official Website</a>
</div>

## Built using

* [Vue.js](https://vuejs.org/)
* [Pinia](https://pinia.vuejs.org/)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Changelog

[View notable changes.](CHANGELOG.md)
