import { defineStore } from "pinia";

type Position = {
  x: number;
  y: number;
};

export const usePositionsStore = defineStore("positions", {
  state: () => ({
    preview: undefined as Position | undefined,
    positions: [] as Position[],
    images: [] as ImageData[],
  }),
  getters: {
    lastPosition({ positions }): Position | undefined {
      return positions[positions.length - 1];
    },
    getImageData({ images }) {
      return images[images.length - 1];
    },
  },
  actions: {
    addPosition(position: Position) {
      this.positions.push(position);
    },
    updateImageData(image: ImageData) {
      this.images.push(image);
    },
    undo() {
      this.images.pop();
      this.positions.pop();
    },
    setPreview(preview?: Position) {
      this.preview = preview;
    },
  },
});
